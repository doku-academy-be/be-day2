#!/bin/sh

name=$1
unamefb=$2
unameln=$3
root="$name at $(date)"

mkdir "$root"

#Directory about_me, personal, and profesional
mkdir "$root"/about_me
mkdir "$root"/about_me/personal
mkdir "$root"/about_me/professional

echo "https://facebook.com/$unamefb" > "$root"/about_me/personal/facebook.txt
echo "https://linkedin.com/in/$unameln" > "$root"/about_me/professional/linkedin.txt

#Directory my_friends
mkdir "$root"/my_friends

echo "$(curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt)" > "$root"/my_friends/list_of_my_friends.txt

#Directory my_system_info
mkdir "$root"/my_system_info

echo "My username : $1" > "$root"/my_system_info/about_this_laptop.txt
echo "$(uname -a)" >> "$root"/my_system_info/about_this_laptop.txt

echo "$(ping -c 3 www.google.com)" >> $root/my_system_info/internet_connection.txt
